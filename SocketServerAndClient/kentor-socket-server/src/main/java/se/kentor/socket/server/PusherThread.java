package se.kentor.socket.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.net.Socket;

import se.kentor.socket.common.Message;

public class PusherThread extends Thread {
    protected Socket socket;
    private InputStream inp = null;
    private BufferedReader brinp = null;
    private ObjectOutputStream out = null;

    public PusherThread(Socket clientSocket) {
        this.socket = clientSocket;
        try {
            inp = socket.getInputStream();
            brinp = new BufferedReader(new InputStreamReader(inp));
            out = new ObjectOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            return;
        }
    }

    public void run() {
        String line;
        while (true) {
            try {
                line = brinp.readLine();
                if ((line == null) || line.equalsIgnoreCase("QUIT")) {
                    socket.close();
                    return;
                } else {
                    out.writeBytes(line + "\n\r");
                    out.flush();
                }
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
        }
    }

    public void sendMessage(Message message) {
        try {
            System.out.println("Sending: " + message.getMessage());
            out.writeObject(message);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
