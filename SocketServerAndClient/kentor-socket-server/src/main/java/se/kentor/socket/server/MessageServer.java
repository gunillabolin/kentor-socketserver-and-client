package se.kentor.socket.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import se.kentor.socket.common.Message;

public class MessageServer implements Runnable {

    ArrayList<PusherThread> threads;
    Socket socket = null;
    ServerSocket serverSocket = null;

    public static void main(String[] args) throws IOException {
        System.out.println("Server started");
        MessageServer messageServer = new MessageServer();
        messageServer.run();
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
        while (true) {
            try {
                System.out.println("Waiting for connection.");
                socket = serverSocket.accept();
                System.out.println("Connection found.");
            } catch (IOException e) {
                System.out.println("I/O error: " + e);
            }
            // new threa for a client
            PusherThread thread = new PusherThread(socket);
            thread.start();
            System.out.println("Adding new Thread");
            threads.add(thread);
        }
    }

    public MessageServer() {
        threads = new ArrayList<PusherThread>();
        try {
            serverSocket = new ServerSocket(6789);
        } catch (IOException e) {
            e.printStackTrace();

        }
    }

    public void sendMessages(Message message) {
        for (PusherThread pusherThread : threads) {
            pusherThread.sendMessage(message);
        }
    }
}
