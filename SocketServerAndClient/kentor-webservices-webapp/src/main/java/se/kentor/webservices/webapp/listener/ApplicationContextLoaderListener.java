package se.kentor.webservices.webapp.listener;

import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import se.kentor.socket.server.MessageServer;

/**
 * Initializes context when starting up Web application (see WEB-INF/web.xml).
 */
public class ApplicationContextLoaderListener extends ContextLoaderListener {
    private Logger log = Logger.getLogger(this.getClass().getPackage().getName());

    public static final MessageServer messageServer = new MessageServer();

    @Override
    public void contextInitialized(ServletContextEvent event) {
        super.contextInitialized(event);
        log.info("======== Application :: Started ========");

        DOMConfigurator config = new DOMConfigurator();
        config.doConfigure(this.getClass().getClassLoader().getResourceAsStream("log4j.xml"), LogManager.getLoggerRepository());

        Thread messageServerThread = new Thread(messageServer);
        messageServerThread.start();

    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        super.contextDestroyed(event);
        log.info("======== Application :: Stopped ========");
    }

    public static final WebApplicationContext getWebRequest(final ServletContext sc) {
        return WebApplicationContextUtils.getWebApplicationContext(sc);
    }

    public static List<String> getActiveProfiles(final ServletContext sc) {
        return Arrays.asList(getWebRequest(sc).getEnvironment().getActiveProfiles());
    }

}
