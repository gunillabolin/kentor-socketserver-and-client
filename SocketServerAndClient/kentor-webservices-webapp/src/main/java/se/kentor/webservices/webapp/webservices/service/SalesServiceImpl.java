package se.kentor.webservices.webapp.webservices.service;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import org.apache.cxf.annotations.Logging;

import se.kentor.socket.common.Message;
import se.kentor.webservices.webapp.listener.ApplicationContextLoaderListener;

@WebService(name = "SalesServicePortType", serviceName = "SalesService", portName = "SalesServiceEndPoint", endpointInterface = "se.kentor.webservices.webapp.webservices.service.SalesService")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@Logging
public class SalesServiceImpl implements SalesService {

    @Override
    public void postSale(Message postSaleRequest) throws IllegalArgumentException {
        ApplicationContextLoaderListener.messageServer.sendMessages(postSaleRequest);
    }

}
