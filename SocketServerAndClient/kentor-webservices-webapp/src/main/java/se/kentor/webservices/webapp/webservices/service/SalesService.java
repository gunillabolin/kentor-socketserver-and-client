package se.kentor.webservices.webapp.webservices.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlElement;

import se.kentor.socket.common.Message;

@WebService(name = "SalesService")
public interface SalesService {

    @WebMethod(operationName = "postSale")
    public void postSale(@WebParam(partName = "postSaleRequest", name = "postSaleRequest") @XmlElement(required = true) final Message postSaleRequest)
            throws IllegalArgumentException;
}
