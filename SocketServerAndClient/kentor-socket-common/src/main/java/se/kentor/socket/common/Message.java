package se.kentor.socket.common;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(namespace = "http://se.kentor.webservices.webapp.webservices.models.request")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Message implements Serializable {

    private static final long serialVersionUID = 1772493688677870239L;

    private String salesPerson;
    private String message;
    private BigDecimal katching;

    public String getSalesPerson() {
        return salesPerson;
    }

    public void setSalesPerson(String salesPerson) {
        this.salesPerson = salesPerson;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public BigDecimal getKatching() {
        return katching;
    }

    public void setKatching(BigDecimal katching) {
        this.katching = katching;
    }

}
