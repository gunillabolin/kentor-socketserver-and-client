package se.kentor.socket.client;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.Properties;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;

import se.kentor.socket.client.exceptions.ClientConfigurationException;
import se.kentor.socket.common.Message;

public class Client {

    private class ConfigData {
        public String serverIp;
        public int serverPort;
    }

    public Client() {
        ConfigData configData = readConfig();
        connect(configData.serverIp, configData.serverPort);
    }

    private ConfigData readConfig() {
        ConfigData result = new ConfigData();

        try (InputStream inputStream = Client.class.getClassLoader().getResourceAsStream("META-INF/config.properties")) {

            if (inputStream == null) {
                throw new ClientConfigurationException("Missing config.properties in META-INF");
            }
            Properties connectionProperties = new Properties();
            try {
                connectionProperties.load(inputStream);
            } catch (IOException e) {
                throw new ClientConfigurationException("Missing config.properties in META-INF"); // FIXME �r inte helt hundra p� att detta �r enda
                                                                                                 // orsaken till att man kan f� ett exception h�r
            }
            result.serverIp = connectionProperties.getProperty("server_ip");
            result.serverPort = Integer.parseInt(connectionProperties.getProperty("server_port"));

        } catch (IOException e1) {
            // FIXME Det h�r inneb�r om jag t�nkt r�tt att n�got gick fel vid close, lugnt att ignorera?
        }

        System.out.println("Config");
        System.out.println("Server ip: " + result.serverIp);
        System.out.println("Server port: " + result.serverPort);
        return result;
    }

    private void connect(String serverIp, int serverPort) {
        try (Socket clientSocket = new Socket(serverIp, serverPort);
             ObjectInputStream objectInputStream = new ObjectInputStream(clientSocket.getInputStream());) {			
            
        	Message message = null;            
            while (true) {
                message = (Message) objectInputStream.readObject();
                if (message != null) {
                    System.out.println("From server: " + message.getMessage() + " vi s�lde f�r: " + message.getKatching() + " h�lsningar, "
                            + message.getSalesPerson());
                    makeSound();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static synchronized void makeSound() {
    	try (AudioInputStream inputStream = AudioSystem.getAudioInputStream(Client.class.getClassLoader().getResourceAsStream("META-INF/airhorn.wav"));) {
    		AudioFormat format = inputStream.getFormat();
            DataLine.Info info = new DataLine.Info(Clip.class, format);
            Clip clip = (Clip) AudioSystem.getLine(info);
            clip.open(inputStream);
            clip.start();
    	} catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new Client();
    }
}
