package se.kentor.socket.client.exceptions;

public class ClientConfigurationException extends RuntimeException {

    public ClientConfigurationException(String string) {
        super(string);
    }

    private static final long serialVersionUID = -7745936248960245048L;

}
